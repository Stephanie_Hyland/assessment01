# Virtualisation Marked - Assessment 1

## Tasks to complete
1. Make a repo on own machine for the assessment that will contain all the files needed to provision the machines etc
2. Write out steps in README of what i need to do
3. Create repo on BitBucket and add remote for this - done
4. . First I will start with launching/provisioning the Ubuntu and AWS linux servers
5. These both have requirements 
      1. Ubuntu <br>
     Name: yourfirstname-ubuntu <br>
     Project: assessment1 <br>
     Ubuntu Server 18.04 LTS <br>
     1GB RAM <br>
     1 vCPU <br>
     Should have NGINX installed <br>

     1. AWS linux <br>
     Name: yourfirstname-aws
     Project: assessment1 <br>
     Amazon Linux 2 AMI (hvm) <br>
     1GB RAM <br>
     1 vCPU <br>
     Should have Apache installed <br>


6. So now i have launched the aws and linux machines
7. need to install apache - this is done on linux by sudo yum -y install httpd
8. need to install nginx for the ubuntu machine - this is done with sudo apt update and sudo apt install nginx
9.  Need to launch these instances on AWS after making sure the requirements are correct - done
10. Both VMd have a user called instructor with a password called 'see google doc for this;
       1. i've successfullly done this for the amazon linux machine - now will try and do this for the ubuntu machine also
       2. before i did sudo useradd -m instructor, sudo passwd instructor, su - instructor then cd /etc/ssh then sudo nano sshd_config, change password authentication to yes, save and load config, then a system reload and i was able to log in as the new user instructor with the password provided
       3. ubuntu machine new username and password - tho first i should check that the script is working
       4. not sure if it is working for the ubuntu su - instructor doesn't seem to work the same 
       5. 
11. next step after is the user for the aws machine - so would i log on to the aws machine to do this
12. from the ifconfig can grab the second column by ifconfig  | awk '{ print $2 }' 
13. the haproxy.sh is to provision the load balancer which is an ubuntu machine
14. the haproxy.sh file should read in the haproxy.cfg file so it uses the round robin method, and i've changed the two private IP adresses for those of the ubuntu server and linux server private ip addresses
15. To run the 3 programs you would:
       1. ./webserver_linux.sh ip_adress_for_linux
       2. ./webserver_ubuntu.sh ip_adress_for_ubuntu
       3. ./haproxy.sh ip_adress_for_loadbalancer