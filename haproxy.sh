#!/bin/bash
if (( $# > 0))
then
  hostname=$1
else
  echo "WTF: you must supply a hostname or IP address" 1>&2
  exit 1
fi
# ubuntu systems log in with ubuntu user
# atp-get might need the repo of available packages to be updated
ssh -o StrictHostKeyChecking=no -i ~/.ssh/StephanieCunnah-HylandKey.pem ubuntu@$hostname '
sudo apt update
sudo apt install nginx
'