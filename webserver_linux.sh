#!/bin/bash
if (( $# > 0))
then
  hostname=$1
else
  echo "WTF: you must supply a hostname or IP address" 1>&2
  exit 1
fi
scp -o StrictHostKeyChecking=no -i ~/.ssh/StephanieCunnah-HylandKey.pem linux.init ec2-user@$hostname:linux.init
ssh -o StrictHostKeyChecking=no -i ~/.ssh/StephanieCunnah-HylandKey.pem ec2-user@$hostname '
sudo yum -y install httpd 

if rpm -qa | grep '^httpd-[0-9]' >/dev/null 2>&1
then
  sudo systemctl start httpd
fi
echo -e "My IP address is $(ifconfig | grep -A1 eth0 | grep inet | awk '{print $2}'| sed 's/^.*://')\n</html>" >>/var/www/html/index.html
sudo sh -c "cat -e >/var/www/html/index.html <<_END_
<h1> Hello there. </h1>
<h2> My private IP address is </h2>
<h2> Stephanie Cunnah-Hyland </h2>
<h2> Name and version of the operating system, obtained from the file /etc/redhat-release) - need to do this </h2>
<h2> The webserver software the system is running and the version of the package installed - hint use rpm and dpkg - need to do this </h2>

_END_"
'
